import re
import sys
import operator
from nltk import SnowballStemmer

def factory(lang):
    if lang == "it":
        return SnowballStemmer("italian")
    if lang == "en":
        return SnowballStemmer("english")
    if lang == "fr":
        print "returning french stemmer"
        return SnowballStemmer("french")
    if lang == "de":
        return SnowballStemmer("german")
    if lang == "sp":
        return SnowballStemmer("spanish")
    print "default stemmer"
    return SnowballStemmer("english")

def openFile(lang):
    if lang == "it":
        return open('minItalian.txt', 'r')
    if lang == "en":
        return open('minEnglish.txt', 'r')
    if lang == "de":
        return open('minGerman.txt', 'r')
    if lang == "fr":
        return open('minFrench.txt', 'r')
    if lang == "sp":
        return open('minSpanish.txt', 'r')
    print "default file"
    return open('minEnglish.txt', 'r')

def findMatch(word, limit, lang):
    toReturn = []
    for i in range(len(word), limit - 1, -1):
        #print i
        #print word[-i:]
        suffix = word[-i:]
        pattern = re.compile(suffix.lower() + '$')
        print "pattern: " + suffix.lower() + '$'

        text = openFile(lang)
        
        for line in text:
            line = line.lower()
            matched = pattern.search(line)
            if matched:
                toReturn.append(line.strip().replace("\\", "/"))
        text.close()
    return toReturn


lang = sys.argv[1]
word = sys.argv[2].lower()
limit = 3

similar = findMatch(word, limit, lang)

#print "firstMatch: " + similar
matches = []
stemmer = factory(lang)
for sim in similar:    
    stemmed = stemmer.stem(sim)
    print "stemmed: " + stemmed

    text = openFile(lang)
    pattern = re.compile('^' + stemmed)
    for line in text:
        line = line.lower().strip()
        matched = pattern.search(line)
        if matched:
            tmpSuffix = line[len(stemmed):]
            print line
            if len(tmpSuffix):
                matches.append(tmpSuffix)
    text.close()

print str(matches)

stemmedInput = stemmer.stem(word)

derivations = {}

for tmp in matches:
    if (stemmedInput + tmp) in derivations:
        derivations[stemmedInput + tmp] = derivations[stemmedInput + tmp] + 1
    else:
        derivations[stemmedInput + tmp] = 1
    
print "======================="

sorted_x = sorted(derivations.iteritems(), key=operator.itemgetter(1), reverse=1)
print str(sorted_x)
for k,v in sorted_x:
    times = int(v)
    dashes = ""
    for i in range(times):
        dashes = dashes + "-"
    print "%s\t\t%s" % (k, dashes)



