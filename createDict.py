import sys

def main(argv):
    """For given size (# of lines) and filename with dictionary (use wc -l)
    it creates reduced dictionary, containing around 5% of original.
    The reduced dictionary is made by slicing input into pieces.
    Each piece is 10 words."""
    
    percentageSize = 0.05
    setSize = 10

    inputName = sys.argv[2]
    inputSize = float(sys.argv[1])
    outputSize = inputSize * percentageSize
    jumps = int(outputSize / setSize)

    ommitSize = int ((inputSize - outputSize)/jumps)

    text = open(inputName, 'r')

    count = 0;

    #print "inputName = %s" % inputName

    #print "inputSize = %d" % inputSize
    #print "outputSize = %d" % outputSize
    #print "jumps = %d" % jumps
    #print "ommitSize = %d" % ommitSize

    collecting = 1

    for line in text:
        if collecting > 0:
            sys.stdout.write("%s" %line.lower())
            count += 1
            if count >= 10:
                count = 0
                collecting = 0
        else:
            if count < ommitSize:
                count += 1
            else:
                count = 0
                collecting = 1

if __name__ == "__main__":
    main(sys.argv)
