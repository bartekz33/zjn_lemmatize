import re
import sys
import operator
from nltk import SnowballStemmer

def stemmerFactory(lang):
    """For given lang it creates and returns proper SnowballStemmer."""
    if lang == "it":
        return SnowballStemmer("italian")
    if lang == "en":
        return SnowballStemmer("english")
    if lang == "fr":
        print "returning french stemmer"
        return SnowballStemmer("french")
    if lang == "de":
        return SnowballStemmer("german")
    if lang == "sp":
        return SnowballStemmer("spanish")
    print "default stemmer"
    return SnowballStemmer("english")

def openDictionary(lang):
    """For given lang it opens proper file and returns pointer to it."""
    if lang == "it":
        return open('minItalian.txt', 'r')
    if lang == "en":
        return open('minEnglish.txt', 'r')
    if lang == "de":
        return open('minGerman.txt', 'r')
    if lang == "fr":
        return open('minFrench.txt', 'r')
    if lang == "sp":
        return open('minSpanish.txt', 'r')
    print "default file"
    return open('minEnglish.txt', 'r')

def findSimilarWords(word, limit, lang):
    """ For given word it searches for similar word based on endings (limit is minimal length of ending).
    """
    toReturn = []
    for i in range(len(word), limit - 1, -1):
        #print i
        #print word[-i:]
        suffix = word[-i:]
        pattern = re.compile(suffix.lower() + '$')
        #print "pattern: " + suffix.lower() + '$'

        text = openDictionary(lang)
        
        for line in text:
            line = line.lower()
            matched = pattern.search(line)
            if matched:
                toReturn.append(line.strip().replace("\\", "/"))
        text.close()
    return toReturn
    
def createEndings(lang, similar):
    """ For given words it creates and returns list of all endings.
    It simply cuts-off the core of the word (using stemmer to find core).
    """
    endings = []
    stemmer = stemmerFactory(lang)
    for sim in similar:    
        stemmed = stemmer.stem(sim)
        #print "stemmed: " + stemmed

        text = openDictionary(lang)
        pattern = re.compile('^' + stemmed)
        for line in text:
            line = line.lower().strip()
            matched = pattern.search(line)
            if matched:
                tmpSuffix = line[len(stemmed):]
                #print line
                if len(tmpSuffix):
                    endings.append(tmpSuffix)
        text.close()
    return endings
    
def createPossibleDerivations(lang, word, endings):
    """For given word and endings it creates frequenced list (dict) of possible endings."""
    derivations = {}
    stemmer = stemmerFactory(lang) 

    stemmedInput = stemmer.stem(word)

    for tmp in endings:
        if (stemmedInput + tmp) in derivations:
            derivations[stemmedInput + tmp] = derivations[stemmedInput + tmp] + 1
        else:
            derivations[stemmedInput + tmp] = 1
    return derivations

def sort(derivations):
    """Used to sort items in dict by values."""
    sorted_x = sorted(derivations.iteritems(), key=operator.itemgetter(1), reverse=1)
    return sorted_x

def printResult(sorted_x):
    """Print freq dict in a fancy way."""
    for k,v in sorted_x:
        times = int(v)
        dashes = ""
        for i in range(times):
            dashes = dashes + "-"
        print "%s\t\t%s" % (k, dashes)

def main(argv):
    lang = argv[1]
    word = argv[2].lower()
    limit = 3

    similar = findSimilarWords(word, limit, lang)

    #print "firstMatch: " + similar
    endings = createEndings(lang, similar)

    derivations = createPossibleDerivations(lang, word, endings)
        
    print "======================="

    sorted_x = sort(derivations)
    printResult(sorted_x)

if __name__ == "__main__":
    main(sys.argv)
    


