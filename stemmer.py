#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
from nltk import SnowballStemmer

def factory():
    lang = sys.argv[1]
    if lang == "it":
        return SnowballStemmer("italian")
    if lang == "en":
        return SnowballStemmer("english")
    if lang == "fr":
        return SnowballStemmer("french")
    if lang == "de":
        return SnowballStemmer("german")
    if lang == "sp":
        return SnowballStemmer("spanish")
    return SnowballStemmer("english")

stemmer = factory()
print stemmer.stem(sys.argv[2].lower())
